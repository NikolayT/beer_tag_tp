-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema beer_tag_db
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `beer_tag_db` ;

-- -----------------------------------------------------
-- Schema beer_tag_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `beer_tag_db` DEFAULT CHARACTER SET utf8 ;
USE `beer_tag_db` ;

-- -----------------------------------------------------
-- Table `beer_tag_db`.`beer_styles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`beer_styles` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`beer_styles` (
  `StyleID` INT(11) NOT NULL AUTO_INCREMENT,
  `StyleName` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`StyleID`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `beer_tag_db`.`countries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`countries` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`countries` (
  `CountryID` INT(11) NOT NULL AUTO_INCREMENT,
  `CountryCode` CHAR(2) NOT NULL,
  `CountryName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`CountryID`))
ENGINE = InnoDB
AUTO_INCREMENT = 247
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `beer_tag_db`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`users` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`users` (
  `UserID` INT(11) NOT NULL AUTO_INCREMENT,
  `AvatarData` LONGBLOB NOT NULL,
  `Username` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`UserID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `beer_tag_db`.`beers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`beers` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`beers` (
  `BeerID` INT(11) NOT NULL AUTO_INCREMENT,
  `BeerName` VARCHAR(30) NOT NULL,
  `AlcoholByVolume` DOUBLE NOT NULL,
  `Description` VARCHAR(255) NULL DEFAULT NULL,
  `PictureData` LONGBLOB NOT NULL,
  `CountryID` INT(11) NOT NULL,
  `BreweryName` VARCHAR(45) NOT NULL,
  `UserID` INT(11) NOT NULL,
  `StyleID` INT(11) NOT NULL,
  `AvgRating` DOUBLE NULL DEFAULT 0.0,
  PRIMARY KEY (`BeerID`),
  CONSTRAINT `fk_beer_beer_styles1`
    FOREIGN KEY (`StyleID`)
    REFERENCES `beer_tag_db`.`beer_styles` (`StyleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beer_origin_countries1`
    FOREIGN KEY (`CountryID`)
    REFERENCES `beer_tag_db`.`countries` (`CountryID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beer_users1`
    FOREIGN KEY (`UserID`)
    REFERENCES `beer_tag_db`.`users` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_beer_origin_countries1_idx` ON `beer_tag_db`.`beers` (`CountryID` ASC);

CREATE INDEX `fk_beer_users1_idx` ON `beer_tag_db`.`beers` (`UserID` ASC);

CREATE INDEX `fk_beer_beer_styles1_idx` ON `beer_tag_db`.`beers` (`StyleID` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_db`.`beer_ratings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`beer_ratings` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`beer_ratings` (
  `RatingID` INT(11) NOT NULL AUTO_INCREMENT,
  `BeerID` INT(11) NOT NULL,
  `UserID` INT(11) NOT NULL,
  `Rating` INT(11) NOT NULL,
  PRIMARY KEY (`RatingID`),
  CONSTRAINT `fk_beer_has_users_beer1`
    FOREIGN KEY (`BeerID`)
    REFERENCES `beer_tag_db`.`beers` (`BeerID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beer_has_users_users1`
    FOREIGN KEY (`UserID`)
    REFERENCES `beer_tag_db`.`users` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_beer_has_users_users1_idx` ON `beer_tag_db`.`beer_ratings` (`UserID` ASC);

CREATE INDEX `fk_beer_has_users_beer1_idx` ON `beer_tag_db`.`beer_ratings` (`BeerID` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_db`.`tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`tags` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`tags` (
  `TagID` INT(11) NOT NULL AUTO_INCREMENT,
  `TagName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`TagID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `beer_tag_db`.`beer_tag_relations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`beer_tag_relations` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`beer_tag_relations` (
  `RelationID` INT(11) NOT NULL AUTO_INCREMENT,
  `BeerID` INT(11) NOT NULL,
  `TagID` INT(11) NOT NULL,
  PRIMARY KEY (`RelationID`),
  CONSTRAINT `fk_beer_has_tags_beer1`
    FOREIGN KEY (`BeerID`)
    REFERENCES `beer_tag_db`.`beers` (`BeerID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beer_has_tags_tags1`
    FOREIGN KEY (`TagID`)
    REFERENCES `beer_tag_db`.`tags` (`TagID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_beer_has_tags_beer1_idx` ON `beer_tag_db`.`beer_tag_relations` (`BeerID` ASC);

CREATE INDEX `fk_beer_has_tags_tags1_idx` ON `beer_tag_db`.`beer_tag_relations` (`TagID` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_db`.`user_beer_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`user_beer_type` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`user_beer_type` (
  `UserBeerTypeID` INT(11) NOT NULL AUTO_INCREMENT,
  `Type` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`UserBeerTypeID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `beer_tag_db`.`beer_type_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`beer_type_status` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`beer_type_status` (
  `UserBeerID` INT(11) NOT NULL AUTO_INCREMENT,
  `UserID` INT(11) NOT NULL,
  `UserBeerTypeID` INT(11) NOT NULL,
  `BeerID` INT(11) NOT NULL,
  PRIMARY KEY (`UserBeerID`),
  CONSTRAINT `fk_user_beers_User_beer_type1`
    FOREIGN KEY (`UserBeerTypeID`)
    REFERENCES `beer_tag_db`.`user_beer_type` (`UserBeerTypeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_beer_users1`
    FOREIGN KEY (`UserID`)
    REFERENCES `beer_tag_db`.`users` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_beer_type_status_beers1`
    FOREIGN KEY (`BeerID`)
    REFERENCES `beer_tag_db`.`beers` (`BeerID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_users_has_beer_users1_idx` ON `beer_tag_db`.`beer_type_status` (`UserID` ASC);

CREATE INDEX `fk_user_beers_User_beer_type1_idx` ON `beer_tag_db`.`beer_type_status` (`UserBeerTypeID` ASC);

CREATE INDEX `fk_beer_type_status_beers1_idx` ON `beer_tag_db`.`beer_type_status` (`BeerID` ASC);


-- -----------------------------------------------------
-- Table `beer_tag_db`.`authority`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `beer_tag_db`.`authority` ;

CREATE TABLE IF NOT EXISTS `beer_tag_db`.`authority` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `authority` VARCHAR(255) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
