package com.telerikacademy.beertag.services.base;

import com.telerikacademy.beertag.models.OriginCountry;

import java.util.List;

public interface CountryService {

    List<OriginCountry> getAll();
}
