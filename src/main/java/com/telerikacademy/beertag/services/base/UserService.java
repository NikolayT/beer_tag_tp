package com.telerikacademy.beertag.services.base;

import com.telerikacademy.beertag.models.*;
import com.telerikacademy.beertag.models.dtos.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    List<User> getAll();

    User getByName(String username);

    User getByID(int id);

    void deleteUser(int userId);

    void create(UserDTO user);

    void editUser(UserDTO newUser, int userToEdit);

    void rateBeer(int userID, int beerID, int rating);

    List<Beer> getMostRankedBeers(int userId);

    List<Beer> getWishedBeers(int userId);

    List<Beer> getDrankBeers(int userId);

    List<Beer> getUsersBeers(int userId);

    void setBeerType(int beerId, int userId, String type);

    int getBeerRating(int userId, int beerId);
}
