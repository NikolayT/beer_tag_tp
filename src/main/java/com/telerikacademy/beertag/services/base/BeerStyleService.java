package com.telerikacademy.beertag.services.base;

import com.telerikacademy.beertag.models.BeerStyle;

import java.util.List;

public interface BeerStyleService {

    List<BeerStyle> getAll();
}
