package com.telerikacademy.beertag.services.impl;

import com.telerikacademy.beertag.models.BeerStyle;
import com.telerikacademy.beertag.repositories.base.BeerStyleRepository;
import com.telerikacademy.beertag.services.base.BeerStyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeerStyleServiceImpl implements BeerStyleService {

    private BeerStyleRepository beerStyleRepository;

    @Autowired
    public BeerStyleServiceImpl(BeerStyleRepository beerStyleRepository) {
        this.beerStyleRepository = beerStyleRepository;
    }

    @Override
    public List<BeerStyle> getAll() {
        return beerStyleRepository.getAll();
    }
}
