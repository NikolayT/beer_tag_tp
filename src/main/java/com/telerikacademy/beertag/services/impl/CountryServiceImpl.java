package com.telerikacademy.beertag.services.impl;

import com.telerikacademy.beertag.models.OriginCountry;
import com.telerikacademy.beertag.repositories.base.CountryRepository;
import com.telerikacademy.beertag.services.base.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<OriginCountry> getAll() {
        return countryRepository.getAll();
    }
}

