package com.telerikacademy.beertag.exceptions;

import javax.security.sasl.AuthenticationException;

public class UserAlreadyExistsException extends RuntimeException {

    public UserAlreadyExistsException(final String msg) {
        super(msg);
    }
}