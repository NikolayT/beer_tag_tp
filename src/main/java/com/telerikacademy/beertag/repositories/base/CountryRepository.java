package com.telerikacademy.beertag.repositories.base;

import com.telerikacademy.beertag.models.OriginCountry;

import java.util.List;

public interface CountryRepository {

    List<OriginCountry> getAll();

    OriginCountry getById(int id);

    OriginCountry getByName(String country);
}
