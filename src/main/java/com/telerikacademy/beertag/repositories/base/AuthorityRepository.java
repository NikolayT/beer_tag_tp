package com.telerikacademy.beertag.repositories.base;

import com.telerikacademy.beertag.models.Authority;
import com.telerikacademy.beertag.models.OriginCountry;

import java.util.List;

public interface AuthorityRepository {

    List<Authority> getAll();

    Authority getById(int id);

    List<Authority> getByAuthority(String authority);

    Authority getByUsername(String username);

    void updateAuthority(Authority authority);
}